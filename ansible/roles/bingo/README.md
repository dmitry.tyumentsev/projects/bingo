Bingo role
=========

Installs Bingo on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - bingo
```
