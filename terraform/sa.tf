locals {
  service-accounts = toset([
    "${var.resource_name_prefix}-instance-sa",
  ])
  instance-sa-roles = toset([
    "monitoring.editor",
  ])
}

resource "yandex_iam_service_account" "service-accounts" {
  for_each = local.service-accounts
  name     = each.key
}

resource "yandex_resourcemanager_folder_iam_member" "instance-roles" {
  for_each  = local.instance-sa-roles
  folder_id = var.yc_folder_id
  member    = "serviceAccount:${yandex_iam_service_account.service-accounts["${var.resource_name_prefix}-instance-sa"].id}"
  role      = each.key
}
