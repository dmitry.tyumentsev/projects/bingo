locals {
  instances = {
    "proxy"  = { cpu_cores = 2, memory_gb = 4 },
    "node01" = { cpu_cores = 2, memory_gb = 2 },
    "node02" = { cpu_cores = 2, memory_gb = 2 },
    "db01"   = { cpu_cores = 2, memory_gb = 2 },
  }
}

module "yc-instance" {
  source                       = "git::https://gitlab.com/dmitry.tyumentsev/terraform-modules/yc-instance.git"
  for_each                     = local.instances
  instance_name                = each.key
  instance_hostname            = each.key
  instance_vpc_subnet_id       = yandex_vpc_subnet.vpc_subnet.id
  is_instance_preemptible      = false
  instance_cores               = each.value.cpu_cores
  instance_memory              = each.value.memory_gb
  instance_image_family        = "ubuntu-2004-lts"
  instance_labels              = { "ansible_group" : replace(each.key, "/\\d\\d/", "") }
  instance_ssh_public_key_path = var.ssh_public_key_path
  service_account_id           = yandex_iam_service_account.service-accounts["${var.resource_name_prefix}-instance-sa"].id
}
