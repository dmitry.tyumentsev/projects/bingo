# Инструкция по запуску

## Инфраструктура
Состоит из четырех виртуальных машин:
1. proxy - машина с nginx, grafana, prometheus. Выступает как ssh-bastion host для ansible.
2. db01 - машина с postgresql
3. node01 - первая машина с приложением
4. node-2 - вторая машина с приложением

Для автоматизации развертывания в Yandex Cloud используется Terraform

`cd terraform`

`ssh-keygen -t ed25519 -a 32 -f id_ssh -C ya`

```
export TF_VAR_yc_iam_token=$(yc iam create-token)    
export TF_VAR_yc_cloud_id=$(yc config get cloud-id)
export TF_VAR_yc_folder_id=$(yc config get folder-id)
```

`terraform init`

`terraform apply`

В output будет ip, его следует на него следует назначить доменное имя.

## Конфигурация серверов

Осуществляется Ansible плейбуком с набором ролей

`cd ansible`

`eval $(ssh-agent) && ssh-add ../terraform/id_ssh`

Добавить нужное доменное имя в инвентори inventories/yc/hosts.yml для машины proxy

Установить роли

`ansible-galaxy install -r requirements.yml `

`ansible-playbook -i inventories/yc/hosts.yml playbook.yml`

#### Плейбук устанавливает:

На каждый сервер [node exporter](https://github.com/prometheus/node_exporter)

##### Proxy
Nginx для балансировки нагрузки между приложениями, кеширования `/long_dummy` и поддержки https.
Роль для сборки nginx из исходников и поддержкой http3 написать не успел.

Сертификат автоматически генерируется и подтверждается c помощью Let's Encrypt.

Prometheus, сконфигурированный, на нужные цели мониторинга.

Grafana, настроенная на prometheus и с готовым дашбордом для bingo и дашбордом от node-exporter.

![dasboard](img/dasboard.png)

Для мониторинга nginx использовались [nginxlog-exporter](https://github.com/martin-helmich/prometheus-nginxlog-exporter) и [nginx-exporter](https://github.com/nginxinc/nginx-prometheus-exporter).

##### DB01
PostgreSQL с пользователем и базой данных.

PgBouncer с режимом Session позволил избавиться от потерянных запросов к БД.

Для мониторинга использовались [postgresql-exporter](https://github.com/prometheus-community/postgres_exporter) и [pgbouncer-exporter](https://github.com/prometheus-community/pgbouncer_exporter).

Для ускорения запроса к /api/session, который делает Join таблиц, были созданы индексы.

##### Node01, Node02
Устанавливает бинарник на сервера.

При изменении бинарника на одной из машин запускается процесс миграции для бд.

Приложение управляется Systemd.

Проверка состояния приложения, происходит с помощью скрипта healthcheck. Нездоровое приложение перезагружается.
Бесплатный nginx, каждую секунду отслеживает подключение к приложению и если его нет, исключает его из балансировки.

#### Что я бы улучшил

Если бы я выполнял задание в своей организации YandexCloud, я бы выключил все входящие порты, кроме 80, 443, 22 на proxy сервере, и с помощью Cloud DNS добавлял бы A запись, что позволило бы связать Terraform и Ansible, для запуска по одной кнопке.
